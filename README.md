# todo-clean-java

A todo app using clean architecture written in Java

# What is the point of this?

Many developers, when learning a new framework, language, etc. will implement a basic app, todo or otherwise, that allows them to understand the new technology better.

What I am doing with this project, is teaching myself the clean architecture by Bob Martin, also known as onion architecture, or sometimes layered architecture.

# Why?

I want to be a better developer than I currently am, I have a bachelors degree, so understand to some degree how computers work, algorithms, data structures, etc.
Also, in my time as a full time developer, I've learned a lot about the software developer methodologies, agile, scrum, even waterfall, etc.
What I have yet to really sink my teeth into, is architecture as a whole, as envisioned by Bob Martin

> How do you get the business rules to use tools without knowing about them?

> You invert the dependency. You have the database depend upon the business rules. You make sure the business rules don’t depend on the database.

> You are talking gibberish.

> On the contrary, I am speaking the language of Software Architecture. This is the Dependency Inversion Principle. Low level policies should depend upon high level policies.

also:

> What do you mean by that?

> Remember that you started by saying that you wanted to be a Software Architect? You wanted to make all the really important decisions?

> Yes, that’s what I want.

> Among those decisions that you wanted to make were the database, and the webserver, and the frameworks.

> Yeah, and you said those weren’t the important decisions. You said they were irrelevant.

> That’s right. They are. The important decisions that a Software Architect makes are the ones that allow you to NOT make the decisions about the database, and the webserver, and the frameworks.

> But you have to make those decisions first!

> No, you don’t. Indeed, you want to be allowed to make them much later in the development cycle – when you have more information. Woe is the architect who prematurely decides on a database, and then finds that flat files would have been sufficient. Woe is the architect who prematurely decides upon a web-server, only to find that all the team really needed was a simple socket interface. Woe is the team whose architects prematurely impose a framework upon them, only to find that the framework provides powers they don’t need and adds constraints they can’t live with. Blessed is the team whose architects have provided the means by which all these decisions can be deferred until there is enough information to make them. Blessed is the team whose architects have so isolated them from slow and resource hungry IO devices and frameworks that they can create fast and lightweight test environments. Blessed is the team whose architects care about what really matters, and defer those things that don’t.


In the eyes of Bob Martin, a good architect, and a good architecture, allow the developers to defer implementation details, once you have enough information to decide on them.

# But a todo app is too simple, it doesn't need "architecture"

Of course not! This is a simple program, but the point is that I'm learning how to do clean architecture on a project that has simple requirements, so I can focus on what clean architecture is, instead of designing core business logic.

# So what does your architecture look like

I'm glad you asked!

In the clean/onion/layered architecture, there are many layers of levels of logic, and the critical business logic is at the highest level of abstraction (away from implementation details). Examples:

The Entities in the system:
1. Todo
1. User
1. UserTodo

The todo object is obvious, it is the core information of the system, holding the data that is the reason we are here
The user object is there to complicate things, to create a multi-table, multi-user system that requires me to think about integrating these things
The userTodo entity is just a wrapper to access which users have access to which todos, it should never be used by the FE, only by the BE in business logic

Each entity has a series of Use cases, which use the Entities provided to achieve some business objective
1. AddTodo: add a todo item to storage (will connect to a user in the meantime)
1. AddUser: add a user to storage
1. GetAllTodos: get all todo items (given a user, somewhat permissions-based)
1. GetTodo: get a todo description (if you have the todo ID)
1. MarkTodoDone: mark a todo as done
1. MarkTodoNotDOne: mark a todo as not done

Each of these use cases will store the Entities in a todoDB, but the trick is that the todoDB is abstracted out of the system, in my case we use an interface:
```java
public interface dbInterface<T extends Entity> {
    Optional<List<T>> getAll();
    Optional<Boolean> save(T t);
    Optional<T> get(UUID id);
}
```

Those optionals are there to allow me to remain null-safe as I program
The dbInterface defines a getAll, save, and get function, that's it, no matter what DB is used, that's all I've needed so far

I have an implementation of the dbInterface called inMemoryDB. It stores the entitites in a Map<UUID, T> map.
The idea is that at a later time, I can decide to implement dbInterface in another todoDB or todoDB-like location (like flat files if needed), I don't have to change the business logic of the system!!!!

Here is a visual:

```

-------------------------------------------------------------------|
                                                      ExternalInteractors  |
                                                        InMemoryDB --> Java.lang.Map
                                                           |       |
                                                           |       |
--------------------------------------------------------------     |
                     |-- Use Cases -----> dbInterface <----| |     |
-------------------- |    AddTodo                            |     |
Entities           | |    AddUser                            |     |
  Todo      <-|    | |    GetAllTodos                        |     |
  User      <------|-|    GetTodo                            |     |
  UserTodo  <-|    |      MarkTodoDone                       |     |
--------------------      MarkTodoNotDone                    |     |
                          UpdateTodo                         |     |
--------------------------------------------------------------     |
--------------------------------------------------------------------

```

The diagram was hopefully more useful than hurtful!
What essentially is going on here is that we have Entities, which live purely at the highest level of the diagram, everything at compile time depends on the Entities
The Entities are primarily used by the use cases, the use cases are the next highest level of the diagram.
The use cases depend on a storage mechanism, a dbInterface, which it will call at runtime, but at compile time, allows it to create its own dependency.
Thus, the inMemoryDB, an example of an implementation detail, will rely on the dbInterface, defined by the Use Cases at compile time, but at runtime, the use cases call the
implemented database.
For a unit test, you can use dbInterface mocks,
for integration tests, you can use the InMemoryDB.

# To come

- I will expand out my use of the User entity, it's not integrated with all Todo use cases yet.
- I will likely implement a form of tenant/permissions or something, just to show it's possible.
- Then I might implement a real DB, just to show it's possible **without changing business logic**.
- I will likely try to implement a simple ui, so I will incorporate a view object, and controllers to manage the view objects.
- If controllers get involved, I can implement an 'internet' level, so as to turn this into a service driven system.
- I might just give up half way at any point, if I feel I've driven home the high level idea into my brain enough

# Lessons to learn
If you like the idea of this architecture, but you are thinking "There is no way we can use this style in our currently running systems, it would take way too long to refactor".
Then you are not alone! If you have plenty of working implemented services/systems, then I would not bother to refactor, one of the greatest things about clean architecture is that it allows you
to defer the big decisions, but if you've already made those decisions (for better or worse...) then about half the reason to implement this is behind you.
However, if you are refactoring some old code, in a way that will not require a re-write, and you wanted to take away some key insights, this is the section for you!

So, the biggest lesson I've learned in writing this app is the SOLID principles of software development, namely:
- S, SRP, Single Responsibility Principle
- O, OCP, Open Closed Principle
- L, LSP, Liskov Substitution Principle
- I, ISP, Interface Segregation Principle
- D, DIP, Dependency Inversion Principle

What clean architecture really did was help me to figure out when/why to apply SOLID principles, lets go over them:
- SRP: Have only one reason to change (So that when you do change, you limit your scope of change)
- OCP: Be open to extension, but closed to modification (So you don't have to change anything so long as your new features don't affect old features)
- LSP: Let child objects be interchangeable based on parent (This should be obvious, but is hardly the case, just make sure child objects are interchangeable when a module uses a parent object)
- ISP: Don't depend on interfaces you don't use (Limit the methods/logic you pull into a module, so that when those change, you don't break by accident)
- DIP: Create your own dependencies, high level objects should not depend on low level details (So that you don't need to care what those dependencies are yet, and/or you can change them out when appropriate, side effect: Testing is 1000x easier!)

Okay, still, that's a lot, what are the REAL takeaways from this project:
1. Interface segregation, seriously, limit your interfaces whenever possible, to avoid cross-contamination
1. Dependency inversion, very seriously, create abstractions of interfaces you depend on, then make your dependencies implement those abstractions

If you are stuck in a tough legacy code system, and you want to make something, *anything* easier, do this:
1. Write unit tests! (Unit as in module-level, not method-level). This allows you to make changes to your system without worrying about accidentally changing business logic!
1. The easiest thing to do is probably to clean up your imports/modules to not depend on things you don't use
1. Try creating abstractions of hard dependencies, then use those abstractions in high level code. This might be hard as it will require more refactoring (so have good tests!)
1. If you have time, pull out your Entities into their own classes. Don't let them implement any DB specific code, Json serialization, NOTHING, absolutely nothing, just data, and critical business logic. This will require rewriting everything that uses your Entities to use both your entity, and probably a database based entity, and possibly a json/REST style entity. It's okay, having 3 versions of your entity around is fine, it allows each object to use only what it needs, and nothing more (ISP).

With these 4 steps, you will be able to find bugs easier, implement new features faster, and will have a better mental model of your app.
