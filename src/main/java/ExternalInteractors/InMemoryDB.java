package ExternalInteractors;

import Entities.Todo;
import Entities.User;
import Entities.UserTodo;
import TodoAppExceptions.TodoFailedToSaveInStorage;
import UseCases.LoadTodo;
import UseCases.SaveTodo;
import UseCases.SaveUser;
import UseCases.SaveUserTodo;
import lombok.var;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class InMemoryDB
        implements SaveTodo.SaveTodoToStorage,
        LoadTodo.LoadUserTodo,
        LoadTodo.LoadTodoFromStorage,
        SaveUserTodo.SaveUserTodoToStorage,
        SaveUser.SaveUserToStorage
    {
    private HashMap<UUID, Todo> todoStorage;
    private HashMap<UUID, User> userStorage;
    private List<UserTodo> userTodoStorage;

    public InMemoryDB() {
        this.todoStorage = new HashMap<>();
        this.userStorage = new HashMap<>();
        this.userTodoStorage = new ArrayList<>();
    }

    @Override public List<UUID> loadTodoIdsGivenUserId(UUID userId) {
        return userTodoStorage.stream()
                .filter(x -> x.getUserId().equals(userId)) // filters out other users
                .map(UserTodo::getTodoId) // gets the ids
                .collect(Collectors.toList());
    }

    @Override public void saveTodo(Todo todo) {
        try {
            todoStorage.put(todo.getId(), todo);
        } catch (Exception e) {
            throw new TodoFailedToSaveInStorage("The todo item failed to `put` to a HashMap<UUID, Todo> in java.");
        }
    }

    @Override public void saveUserTodo(UserTodo userTodo) {
        try {
            userTodoStorage.add(userTodo);
        } catch (Exception e) {
            throw new TodoFailedToSaveInStorage("The userTodo item failed to `put` to a HashMap<UUID, UserTodo> in java.");
        }
    }

    @Override
    public void saveUser(User user) {
        try {
            userStorage.put(user.getId(), user);
        } catch (Exception e) {
            throw new TodoFailedToSaveInStorage("The user item failed to `put` to a HashMap<UUID, User> in java.");
        }
    }

        @Override
        public Todo loadTodo(UUID id) {
            return this.todoStorage.get(id);
        }
    }
