package UseCases;

import Entities.Todo;
import TodoAppExceptions.TodoFailedToSaveInStorage;
import TodoAppExceptions.TodoFailedToLoadFromStorage;
import lombok.AllArgsConstructor;

import java.util.UUID;

@AllArgsConstructor
public class MarkTodoDone {
    private LoadTodo.LoadTodoFromStorage loadUserFromStorage;
    private SaveTodo.SaveTodoToStorage saveTodoToStorage;

    public void markTodoDone(UUID todoId) throws TodoFailedToLoadFromStorage, TodoFailedToSaveInStorage {
        Todo todo = loadUserFromStorage.loadTodo(todoId);
        todo.setDone(true);
        saveTodoToStorage.saveTodo(todo);
    }
}
