package UseCases;

import Entities.Todo;
import TodoAppExceptions.AuthorizationFailure;
import TodoAppExceptions.EmptyTodos;
import lombok.AllArgsConstructor;

import java.util.List;
import java.util.UUID;

@AllArgsConstructor
public class LoadTodo {
    public interface LoadTodoFromStorage {
        Todo loadTodo(UUID id);
    }

    public interface LoadUserTodo {
        List<UUID> loadTodoIdsGivenUserId(UUID userId);
    }

    private LoadTodoFromStorage loadTodoFromStorage;
    private LoadUserTodo userTodoStorage;

    public Todo loadTodo(UUID todoId, UUID userId) {
        List<UUID> todoIds = userTodoStorage.loadTodoIdsGivenUserId(userId);

        if (todoIds.size() == 0) {
            throw new EmptyTodos("That user does not have any todo items");
        }

        if (todoIds.contains(todoId)) {
            return loadTodoFromStorage.loadTodo(todoId);
        } else {
            throw new AuthorizationFailure("User does not have access to that Todo item");
        }
    }
}
