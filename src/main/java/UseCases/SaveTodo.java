package UseCases;

import Entities.Todo;
import Entities.User;
import Entities.UserTodo;
import TodoAppExceptions.TodoFailedToSaveInStorage;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class SaveTodo {
    public interface SaveTodoToStorage {
        void saveTodo(Todo todo) throws TodoFailedToSaveInStorage;
    }

    private SaveTodoToStorage saveTodoToStorage;
    private SaveUserTodo.SaveUserTodoToStorage saveUserTodoToStorage;

    public void saveTodo(Todo todo, User user) throws TodoFailedToSaveInStorage {
        saveTodoToStorage.saveTodo(todo);
        UserTodo userTodo = new UserTodo(user.getId(), todo.getId());
        saveUserTodoToStorage.saveUserTodo(userTodo);
    }
}
