package UseCases;

import Entities.UserTodo;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class SaveUserTodo {
    public interface SaveUserTodoToStorage {
        void saveUserTodo(UserTodo userTodo);
    }

    private SaveUserTodoToStorage storage;

    public void saveUserTodo(UserTodo userTodo) {
        storage.saveUserTodo(userTodo);
    }
}
