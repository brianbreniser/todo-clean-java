package UseCases;

import Entities.User;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class SaveUser {
    public interface SaveUserToStorage {
        void saveUser(User user) throws RuntimeException;
    }

    private SaveUserToStorage saveUserToStorage;

    public void saveUser(User user) throws RuntimeException {
        saveUserToStorage.saveUser(user);
    }
}
