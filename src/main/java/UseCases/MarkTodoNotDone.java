package UseCases;

import Entities.Todo;
import TodoAppExceptions.TodoFailedToLoadFromStorage;
import TodoAppExceptions.TodoFailedToSaveInStorage;
import lombok.AllArgsConstructor;

import java.util.UUID;

@AllArgsConstructor
public class MarkTodoNotDone {
    private LoadTodo.LoadTodoFromStorage loadUserFromStorage;
    private SaveTodo.SaveTodoToStorage saveTodoToStorage;

    public void markTodoNotDone(UUID todoId) throws TodoFailedToLoadFromStorage, TodoFailedToSaveInStorage {
        Todo todo = loadUserFromStorage.loadTodo(todoId);
        todo.setDone(false);
        saveTodoToStorage.saveTodo(todo);
    }
}
