package TodoAppExceptions;

public class UserNotFoundInStorage extends RuntimeException {
    public UserNotFoundInStorage(String message) {
        super(message);
    }
}
