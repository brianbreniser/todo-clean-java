package TodoAppExceptions;

public class EmptyTodos extends RuntimeException {
    public EmptyTodos(String message) {
        super(message);
    }
}
