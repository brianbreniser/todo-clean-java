package TodoAppExceptions;

public class AuthorizationFailure extends RuntimeException {
    public AuthorizationFailure(String message) {
        super(message);
    }
}
