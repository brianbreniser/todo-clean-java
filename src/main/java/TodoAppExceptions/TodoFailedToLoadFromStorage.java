package TodoAppExceptions;

public class TodoFailedToLoadFromStorage extends RuntimeException {
    public TodoFailedToLoadFromStorage(String message) {
        super(message);
    }
}
