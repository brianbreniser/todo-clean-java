package TodoAppExceptions;

public class TodoFailedToSaveInStorage extends RuntimeException {
    public TodoFailedToSaveInStorage(String message) {
        super(message);
    }
}
