package Entities;

import lombok.Data;

import java.util.UUID;

@Data
public class User {
    private UUID id = UUID.randomUUID();
    private String email = "";
    private String password = ""; // Lol security (hey, it's an example app...)

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }
}
