package Entities;

import lombok.Data;

import java.util.UUID;

@Data
public class Todo {
    private UUID id = UUID.randomUUID();
    private String description;
    private Boolean done = false;

    public Todo(String description) {
        this.description = description;
    }
}
