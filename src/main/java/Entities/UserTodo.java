package Entities;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.UUID;

@AllArgsConstructor
@Getter
@EqualsAndHashCode
public class UserTodo {
    private UUID id = UUID.randomUUID();
    private UUID userId;
    private UUID todoId;

    public UserTodo(UUID userId, UUID todoId) {
        this.userId = userId;
        this.todoId = todoId;
    }
}
