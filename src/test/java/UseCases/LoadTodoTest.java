package UseCases;

import Entities.Todo;
import TodoAppExceptions.AuthorizationFailure;
import TodoAppExceptions.EmptyTodos;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class LoadTodoTest {
    @Mock private LoadTodo.LoadUserTodo loadUserTodo;
    @Mock private LoadTodo.LoadTodoFromStorage loadTodoFromStorage;

    private LoadTodo loadTodo;

    @BeforeMethod
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        loadTodo = new LoadTodo(loadTodoFromStorage, loadUserTodo);
    }

    @Test(expectedExceptions = EmptyTodos.class)
    public void testLoadTodoFailsWithEmptyTodosWhenNoTodosExist() {
        // setup
        List<UUID> todoIds = new ArrayList<>();

        when(loadUserTodo.loadTodoIdsGivenUserId(any())).thenReturn(todoIds);
        when(loadTodoFromStorage.loadTodo(any())).thenReturn(new Todo(""));

        // action
        loadTodo.loadTodo(UUID.randomUUID(), UUID.randomUUID());

        // exception expected
    }


    @Test(expectedExceptions = AuthorizationFailure.class)
    public void testLoadTodoFailsWithAuthorizationExceptionWhenUserhasTodosButNotThatOne() {
        // setup
        Todo todo = new Todo("desc");

        List<UUID> todoIds = new ArrayList<UUID>() {{
            add(todo.getId());
        }};

        when(loadUserTodo.loadTodoIdsGivenUserId(any())).thenReturn(todoIds);
        when(loadTodoFromStorage.loadTodo(any())).thenReturn(todo);

        // action
        Todo returnedTodo = loadTodo.loadTodo(UUID.randomUUID(), UUID.randomUUID());

        // exception expected
        assertEquals(returnedTodo, todo);
    }
}