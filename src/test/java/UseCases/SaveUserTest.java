package UseCases;

import Entities.User;
import lombok.val;
import org.mockito.Mockito;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.testng.Assert.*;

public class SaveUserTest {

    private SaveUser.SaveUserToStorage saveUserToStorage;
    private SaveUser saveUser;

    @BeforeMethod
    public void setUp() {
        this.saveUserToStorage = Mockito.mock(SaveUser.SaveUserToStorage.class);
        this.saveUser = new SaveUser(saveUserToStorage);
    }

    @Test
    public void testSaveUserPassesWhenNoExceptionsThrownFromTheDB() {
        // setup
        doNothing().when(saveUserToStorage).saveUser(any());

        // call
        saveUser.saveUser(new User("email", "password"));

        // no exceptions means the test passses
    }

    @Test(expectedExceptions = RuntimeException.class)
    public void testSaveUserFailsWithAppropriateExceptionWhenDBFails() {
        // setup
        doThrow(new RuntimeException("")).when(saveUserToStorage).saveUser(any());

        // call
        saveUser.saveUser(new User("email", "password"));
    }

    @Test
    public void testSaveTodoThrowsCorrectMessageWhenMessageIsThrownFromFailedDBCall() {
        // setup
        val msg = "Saving to db failed";
        doThrow(new RuntimeException(msg)).when(saveUserToStorage).saveUser(any());

        // call
        try {
            saveUser.saveUser(new User("", ""));
        } catch (RuntimeException e) {
            assertEquals(e.getMessage(), msg);
        }
    }
}
