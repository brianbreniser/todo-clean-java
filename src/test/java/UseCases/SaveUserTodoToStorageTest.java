package UseCases;

import Entities.UserTodo;
import lombok.val;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.UUID;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.testng.Assert.assertEquals;

public class SaveUserTodoToStorageTest {
    private SaveUserTodo saveUserTodo;
    private SaveUserTodo.SaveUserTodoToStorage saveUserTodoToStorage;

    @BeforeMethod
    public void setUp() {
        saveUserTodoToStorage = mock(SaveUserTodo.SaveUserTodoToStorage.class);
        saveUserTodo = new SaveUserTodo(saveUserTodoToStorage);
    }

    @Test
    public void testSaveUserTodoDoesNotFailOnNoExceptions() {
        // setup
        doNothing().when(saveUserTodoToStorage).saveUserTodo(any());

        // call
        saveUserTodo.saveUserTodo(new UserTodo(UUID.randomUUID(), UUID.randomUUID()));

        // pass on no exceptions
    }

    @Test(expectedExceptions = RuntimeException.class)
    public void testCreateTodoThrowsExceptionWhenStorageThrowsException() {
        // setup
        doThrow(new RuntimeException("")).when(saveUserTodoToStorage).saveUserTodo(any());

        // call
        saveUserTodo.saveUserTodo(new UserTodo(UUID.randomUUID(), UUID.randomUUID()));
    }

    @Test
    public void testSaveTodoThrowsExceptionWithTheCorrectMessageWhenExceptionIsThrown() {
        // setup
        val msg = "Saving to db failed";
        doThrow(new RuntimeException(msg)).when(saveUserTodoToStorage).saveUserTodo(any());

        // call
        try {
            saveUserTodo.saveUserTodo(new UserTodo(UUID.randomUUID(), UUID.randomUUID()));
        } catch (RuntimeException e) {
            assertEquals(e.getMessage(), msg);
        }
    }
}