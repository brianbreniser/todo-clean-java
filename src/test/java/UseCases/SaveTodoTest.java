package UseCases;

import Entities.Todo;
import Entities.User;
import TodoAppExceptions.TodoFailedToSaveInStorage;
import lombok.val;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.testng.Assert.assertEquals;

public class SaveTodoTest {
    private SaveTodo.SaveTodoToStorage saveTodoToStorage;
    private SaveUserTodo.SaveUserTodoToStorage saveUserTodoToStorage;
    private SaveTodo saveTodo;

    @BeforeClass
    public void setUp() {
        this.saveTodoToStorage = mock(SaveTodo.SaveTodoToStorage.class);
        this.saveUserTodoToStorage = mock(SaveUserTodo.SaveUserTodoToStorage.class);
        saveTodo = new SaveTodo(saveTodoToStorage, saveUserTodoToStorage);
    }

    @Test
    public void testSaveTodoDoesNotThrowAnyExcpetionsOnSuccess() throws TodoFailedToSaveInStorage {
        // setup
        doNothing().when(saveTodoToStorage).saveTodo(any());
        doNothing().when(saveUserTodoToStorage).saveUserTodo(any());

        // call
        saveTodo.saveTodo(new Todo(""), new User("", ""));

        // test should pass if no exceptions thrown
    }

    @Test(expectedExceptions = TodoFailedToSaveInStorage.class)
    public void testCreateTodoThrowsExceptionWhenStorageThrowsException() throws TodoFailedToSaveInStorage {
        // setup
        doThrow(new TodoFailedToSaveInStorage("")).when(saveTodoToStorage).saveTodo(any());
        doNothing().when(saveUserTodoToStorage).saveUserTodo(any());

        // call
        saveTodo.saveTodo(new Todo(""), new User("", ""));
    }

    @Test
    public void testSaveTodoThrowsExceptionWithTheCorrectMessageWhenExceptionIsThrown() throws TodoFailedToSaveInStorage {
        // setup
        val msg = "Saving to db failed";
        doThrow(new TodoFailedToSaveInStorage(msg)).when(saveTodoToStorage).saveTodo(any());
        doNothing().when(saveUserTodoToStorage).saveUserTodo(any());

        // call
        try {
            saveTodo.saveTodo(new Todo(""), new User("", ""));
        } catch (TodoFailedToSaveInStorage e) {
            assertEquals(e.getMessage(), msg);
        }
    }

    @Test(expectedExceptions = TodoFailedToSaveInStorage.class)
    public void testCreateTodoThrowsExceptionWhenUserTodoStorageThrowsException() throws TodoFailedToSaveInStorage {
        // setup
        doNothing().when(saveTodoToStorage).saveTodo(any());
        doThrow(new TodoFailedToSaveInStorage("")).when(saveUserTodoToStorage).saveUserTodo(any());

        // call
        saveTodo.saveTodo(new Todo(""), new User("", ""));
    }

    @Test
    public void testSaveTodoThrowsExceptionWithTheCorrectMessageWhenExceptionIsThrownFromUserTodoStorage()
        throws TodoFailedToSaveInStorage
    {
        // setup
        val msg = "Saving to db failed";
        doNothing().when(saveTodoToStorage).saveTodo(any());
        doThrow(new TodoFailedToSaveInStorage(msg)).when(saveUserTodoToStorage).saveUserTodo(any());

        // call
        try {
            saveTodo.saveTodo(new Todo(""), new User("", ""));
        } catch (TodoFailedToSaveInStorage e) {
            assertEquals(e.getMessage(), msg);
        }
    }
}