package UseCases;

import Entities.Todo;
import TodoAppExceptions.TodoFailedToLoadFromStorage;
import TodoAppExceptions.TodoFailedToSaveInStorage;
import lombok.val;
import org.mockito.Mockito;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.testng.Assert.assertEquals;

public class MarkTodoDoneTest {
    private SaveTodo.SaveTodoToStorage saveSaveTodoToStorage;
    private LoadTodo.LoadTodoFromStorage loadLoadTodoFromStorage;
    private MarkTodoDone markTodoDone;

    @BeforeMethod
    public void setUp() {
        saveSaveTodoToStorage = Mockito.mock(SaveTodo.SaveTodoToStorage.class);
        loadLoadTodoFromStorage = Mockito.mock(LoadTodo.LoadTodoFromStorage.class);
        markTodoDone = new MarkTodoDone(loadLoadTodoFromStorage, saveSaveTodoToStorage);
    }

    @Test
    public void testMarkTodoDoneDoesNotThrowExceptionOnSuccess()
            throws TodoFailedToLoadFromStorage, TodoFailedToSaveInStorage
    {
        // setup
        Todo todo = new Todo("");
        when(loadLoadTodoFromStorage.loadTodo(any())).thenReturn(todo);
        doNothing().when(saveSaveTodoToStorage).saveTodo(any());

        // call
        markTodoDone.markTodoDone(todo.getId());
    }

    @Test
    public void testMarkTodoThrowsExceptionWithCorrectMessageWhenFailedToLoad()
            throws TodoFailedToLoadFromStorage, TodoFailedToSaveInStorage
    {
        // setup
        val msg = "failed to load todo";
        Todo todo = new Todo("");
        doThrow(new TodoFailedToLoadFromStorage(msg)).when(loadLoadTodoFromStorage).loadTodo(any());
        doNothing().when(saveSaveTodoToStorage).saveTodo(any());

        // call
        try {
            markTodoDone.markTodoDone(todo.getId());
        } catch (TodoFailedToLoadFromStorage e) {
            assertEquals(e.getMessage(), msg);
        }
    }

    @Test
    public void testMarkTodoThrowsExceptionWithCorrectMessageWhenFailedToSave()
            throws TodoFailedToLoadFromStorage, TodoFailedToSaveInStorage
    {
        // setup
        val msg = "failed to load todo";
        Todo todo = new Todo("");
        when(loadLoadTodoFromStorage.loadTodo(any())).thenReturn(todo);
        doThrow(new TodoFailedToSaveInStorage(msg)).when(saveSaveTodoToStorage).saveTodo(any());

        // call
        try {
            markTodoDone.markTodoDone(todo.getId());
        } catch (TodoFailedToSaveInStorage e) {
            assertEquals(e.getMessage(), msg);
        }
    }
}